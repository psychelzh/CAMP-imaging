# 0.2.1

* Renamed the project as `CAMP-imaging`. This will be more informative for the future development of the project.
* Supported fixation without triggering. Now all the fixation screen will be displayed without waiting for the scanner to trigger start, and will remain on screen before the experimenter presses `'Escape'` key([#18](https://github.com/psychelzh/CAMP-imaging/issues/18)).
* Added a wait-to-end screen after the experiment of two-back task([#19](https://github.com/psychelzh/CAMP-imaging/issues/19)).
* Enhanced some internal structures of the files.

# 0.2.0

* Removed post-test ([#12](https://github.com/psychelzh/wm-fmri/issues/12)).
* Enhanced subject management and ui logic.
* Removed contents of post recognition test and all the raw stimuli.
* Added association memory test.

# 0.1.1

* Fix practice face images.
* Skip sync tests in practice phase.

# 0.1.0

* Add `"rest"` condition to two-back sequence file ([#8](https://github.com/psychelzh/wm-fmri/issues/8)).
* Add `exp.start_fixation()` for fixation display during resting or structure imaging phases.
* Add a uers interface to aid in the administration of the experiment ([#3](https://github.com/psychelzh/wm-fmri/issues/3)).
* Update stimuli based on recent pilot study [#10](https://github.com/psychelzh/wm-fmri/issues/10).

# 0.0.4

* Enhance stimuli quality: remove watermark and enhance resolution.

# 0.0.3

* Add one similar stimuli for all the stimuli presented in 2-back test.

# 0.0.1.9000

* Added a `Changelog.md` file to track changes.
